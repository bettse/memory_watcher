FROM golang:latest

RUN go get github.com/fabianlee/golang-memtest

WORKDIR /go/src/app
COPY . .
RUN go get -d -v ./...
RUN go install -v ./...

CMD ["memory_watcher"]
