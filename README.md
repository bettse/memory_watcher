# memory_watcher

golang program that registers a cgroup oom watcher and then runs golang-memtest


## Run

(on macos)

1. `docker run -it --cap-add=SYS_ADMIN -e "container=docker" -v /sys/fs/cgroup:/sys/fs/cgroup  -v $GOPATH:/golang -e GOPATH=/gopath -m 100m golang:latest /bin/bash`
2. Edit main.go and replace cgroup path with correct container id
3. (inside container) `go get github.com/fabianlee/golang-memtest`
4. (inside container) `cd /golang/src/gitlab.com/bettse/memory_watcher`
5. (inside container) `go run main.go`


## Docker

1. `docker build -t $(whoami)/$(basename `pwd`) .`
2. `docker run -it --rm --cap-add=SYS_ADMIN -e "container=docker" -v /sys/fs/cgroup:/sys/fs/cgroup -m 100m bettse/memory_watcher`
