module gitlab.com/bettse/memory_watcher

go 1.13

require (
	github.com/containerd/cgroups v0.0.0-20200824123100-0b889c03f102
	golang.org/x/sys v0.0.0-20200124204421-9fbb57f87de9
)
