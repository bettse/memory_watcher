package main

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/containerd/cgroups"
)

func ExampleCommandContext() {
	ctx, cancel := context.WithTimeout(context.Background(), 60*1000*time.Millisecond)
	defer cancel()

	fmt.Printf("start ExampleCommandContext\n")
	if err := exec.CommandContext(ctx, "golang-memtest", "200").Run(); err != nil {
		fmt.Printf("err: %+v %+v\n", err, ctx.Err())
	}
	fmt.Printf("end ExampleCommandContext\n")
}

func main() {
	cpuset, err := exec.Command("cat", "/proc/1/cpuset").Output()
	if err != nil {
		fmt.Printf("err getting cpuset %+v\n", err)
		os.Exit(1)
	}

	cgroup := strings.Trim(string(cpuset), "\t\n ")

	control, err := cgroups.Load(cgroups.V1, cgroups.StaticPath(cgroup))
	if err != nil {
		fmt.Printf("cgroups.Load err %+v\n", err)
		os.Exit(1)
	}

	efd, err := control.OOMEventFD()
	if err != nil {
		fmt.Printf("control.RegisterMemoryEvent err %+v\n", err)
		os.Exit(1)
	}

	oomFile := os.NewFile(efd, "oomFile")
	defer oomFile.Close()

	go readMemoryEvents(oomFile)
	ExampleCommandContext()
}

func readMemoryEvents(efdFile *os.File) {
	fmt.Printf("readMemoryEvents\n")
	// Buffer must be >= 8 bytes for eventfd reads
	// http://man7.org/linux/man-pages/man2/eventfd.2.html
	buf := make([]byte, 8)
	for {
		if _, err := efdFile.Read(buf); err != nil {
			fmt.Printf("efdFile.Read err: %+v\n", err)
			return
		}

		msg := "memory usage for cgroup exceeded threshold"
		fmt.Printf("%+v\n", msg)

		time.Sleep(time.Second)
	}
}
